const langs = ["pl", "ru"];
var ctr, id, toFade;

document.addEventListener('DOMContentLoaded', function() {
    init();
}, false);

function init(){
    // Initializes page.
    hideAllContentDivs();
    showSpecificDiv("home");
    changeLanguage();
    const lang = document.getElementById("language");
    changeLanguageForAll(lang.value);
    modifyContentDivs(lang.value);
    markMobileMenu("home");
}

function hideAllContentDivs(){
    // Hides all content divs.
    document.querySelectorAll(".content-div").forEach((elem) =>{
        elem.style.display = "none";
    });
}

function showSpecificDiv(id){
    // Shows specific div id.
    hideAllContentDivs(); // Hide all contents firstly
    let div = document.getElementById(id);
    let lang = document.getElementById("language").value;
    unfade(div);
    div.style.display = "";
    try{
        let divs = div.getElementsByClassName("text")[0].getElementsByTagName("div")
        for(let i=0; i<divs.length; i++){
            if(!divs[i].className.includes(lang)){
                divs[i].style.display = "none";
            }else {
               divs[i].style.display = "";
               if (divs[i].className.includes("about") && window.screen.width >= 800){
                    console.log(window.screen.width)
                    let height = document.getElementById("about").offsetHeight;
                    document.getElementById("bottom").style.marginTop = (height + 100) + "px";
               } else{
                    document.getElementById("bottom").style.marginTop = "0px";
               }
            }
        }
    }catch{}
    if(div.id == "privacy" || div.id == "contact"){
        document.getElementById("content").style.backgroundColor = "rgb(42,124,111)";
    } else{
        document.getElementById("content").style.backgroundColor = "rgb(44,141,126)";
    }
}

function changeLanguage(){
    // Changes language any time it is clicked.
    const boxes = document.getElementsByClassName("lang");
    Array.from(boxes).forEach((elem) => {
        elem.addEventListener("change", function() {
            changeLanguageForAll(elem.value);
            modifyContentDivs(elem.value);
            Array.from(document.getElementsByClassName("lang")).forEach((div) => {
                div.value = elem.value;
            });
            if(elem.id == "language-mobile") slideOut("mobile-submenu", -600, 0);
        });
    });
}

function modifyContentDivs(lang){
    // Changes language of content-divs.
    Array.from(document.getElementsByClassName("service-description")).forEach((elem) => {
        Array.from(elem.getElementsByTagName("div")).forEach((div) => {
            if(!div.className.includes(lang)){
                div.style.display = "none";
            }else{
                div.style.display = "";
            }
        });
    });
}

function changeLanguageForAll(selector){
    // Changes visibility of lang divs.
    langs.forEach((lang) => {
        Array.from(document.getElementsByClassName(lang)).forEach((tag) => {
            if(selector.includes(lang)) tag.style.display = "";
            else tag.style.display = "none";
        });
    });
}

function showDivInMobile(div){
    // Changes tabs in mobile version
    showSpecificDiv(div);
    markMobileMenu(div);
    slideOut("mobile-submenu", -600, 0);
}

function markMobileMenu(div){
    // Marks colors of specific elems in menu
    Array.from(document.getElementById("mobile-ul").getElementsByTagName("li")).forEach((elem) => {
        elem.style.color = "black";
        if (div+"-li" == elem.id) elem.style.color = "#2c8d7e";
    });
}

function disableScroll() {
    // To get the scroll position of current webpage
    TopScroll = window.pageYOffset || document.documentElement.scrollTop;
    LeftScroll = window.pageXOffset || document.documentElement.scrollLeft,
    
    // if scroll happens, set it to the previous value
    window.onscroll = function() {
    window.scrollTo(LeftScroll, TopScroll);
            };
    }
    
function enableScroll() {
    window.onscroll = function() {};
    }

// Animations

function unfade(element) {
    var op = 0.01;  // initial opacity
    element.style.opacity = op;
    element.style.display = '';
    var timer = setInterval(function () {
        if (op >= 1){
            clearInterval(timer);
        }
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op += op * 0.07;
    }, 10);
}

function slideIn(div, starting, max=0){
    // Allows sliding in of element
    disableScroll();
    var elem = document.getElementById(div);
    var pos = starting;
    elem.style.marginLeft = pos + "px";
    elem.style.display = "block";
    clearInterval(id);
    id = setInterval(frame, 1);
    function frame() {
        if (pos >= max) {
        clearInterval(id);
        elem.style.marginLeft = max + 'px';
        } else {
        pos = pos + 4;
        elem.style.marginLeft = pos + 'px';
        }
    }
}

function slideOut(div, ending, starting=0){
    // Allows sliding in of element
    enableScroll();
    var elem = document.getElementById(div);
    var pos = starting;
    elem.style.marginLeft = pos + "px";
    clearInterval(id);
    id = setInterval(frame, 1);
    function frame() {
        if (pos <= ending) {
        clearInterval(id);
        elem.style.marginLeft = ending + 'px';
        } else {
        pos = pos - 4;
        elem.style.marginLeft = pos + 'px';
        }
    }
}
